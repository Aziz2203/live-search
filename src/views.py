from django.http.response import JsonResponse
from django.shortcuts import render
from src.models import Product


def autocomplete(request):
    if 'term' in request.GET:
        qs = Product.objects.filter(title__icontains=request.GET.get('term'))
        titles = list()
        for product in qs:
            titles.append(product.title)
        return JsonResponse(titles, safe=False)
    product = Product.objects.all()
    context = {
        'product':product,
    }
    return render(request, 'index.html', context)